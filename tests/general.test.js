const app = require('../server')
const chai = require('chai')
const chaiHttp = require('chai-http')
const { before } = require('mocha')
const { expect } = require('chai')

chai.should()
chai.use(chaiHttp)

describe('General test cases', ()=>{
    before((done)=>{
        console.log("just testing before")
        done()
    })
    beforeEach((done)=>{
        console.log('Just testing beforeEach');
        done()
    })

    it("it should test root route i.e app/",(done)=>{
        chai.request(app)
        .get('/app')
        .end((err, response)=>{
            if(err){
                expect().to.throw('This is an error')
            }
            let res = response.body
            response.should.have.status(200);
            res.should.be.a('object');
            res.should.have.property('message').eql('Success message');
            done();
        })
    })

    
    it("Second Test just for testing/",(done)=>{
        chai.request(app)
        .get('/app')
        .end((err, response)=>{
            if(err){
                throw err
            }
            let res = response.body
            response.should.have.status(200);
            res.should.be.a('object');
            res.should.have.property('message').eql('Success message');
            done();
        })
    })

    after((done)=>{
        console.log("Just testing after")
        done()
    })

    afterEach((done)=>{
        console.log("Just testing after each");
        done()
    })
})