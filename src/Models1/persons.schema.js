const mongoose = require('mongoose')

const PersonsSchema = new mongoose.Schema(
    {
        index: Number,
        name: String,
        isActive:Boolean,
        registered: Date,
        age: Number,
        gender: String,
        eyeColor: String,
        favoriteFruit : String,
        company:{
            title: String,
            email:String,
            phone: String,
            location:{
                country: String,
                address: String
            }
        },
        tags:Array
    }
)


const Persons = mongoose.model('persons', PersonsSchema);
module.exports = Persons;
