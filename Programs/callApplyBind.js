
const json1 = {
    fname : 'krishna',
    lname : 'kungiri'
}

const json2 = {
    fname: 'arti',
    lname: 'ghodake'
}



const newFuntion = function(city, state){
    console.log(this.fname + " " + this.lname, city , state)
}

//Call
// newFuntion.call(json1, 'hyderabad', 'telangana')

// newFuntion.call(json2, 'solapur', 'maharashtra')

//Apply
// newFuntion.apply(json1, ['Hyderabad', 'Telangana'])
// newFuntion.apply(json2, ['solapur', 'Maharashtra'])

//Bind
// const json1BindFunction = newFuntion.bind(json1,'hyderabad', 'telangana')
// console.log(json1BindFunction())