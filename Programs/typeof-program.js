// var stringConstructor = "test".constructor;
// var arrayConstructor = [].constructor;
// var objectConstructor = ({}).constructor;

// function whatIsIt(object) {
//     if (object === null) {
//         return "null";
//     }
//     if (object === undefined) {
//         return "undefined";
//     }
//     if (object.constructor === stringConstructor) {
//         return "String";
//     }
//     if (object.constructor === arrayConstructor) {
//         return "Array";
//     }
//     if (object.constructor === objectConstructor) {
//         return "Object";
//     }
//     {
//         return "don't know";
//     }
// }

// var testSubjects = ["string", [1,2,3], {foo: "bar"}, 4];

// for (var i=0, len = testSubjects.length; i < len; i++) {
//     console.log(whatIsIt(testSubjects[i]));
// }