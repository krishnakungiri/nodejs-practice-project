let items = [];
function createOrder(newitem) {
    let flag  = 10
  return new Promise(function (resolve, reject) {
    setTimeout(() => {
        if(flag==10){
            resolve("this is a order creating function")
        }else{
            let parameter = ["this is not 10 ", "number its error"]
            reject(parameter)
        }
    }, 2000);
  });
}

function createPayment() {
    return new Promise(function (resolve, reject) {
        setTimeout(() => {
            resolve("this is a payemnt fucntions") ;
        },3000)
    }) 
}

function sendNotification(){
    return new Promise(function (resolve, reject) {
        setTimeout(() => {
            resolve("this is a notification function")
        },1000)
    })
}

function sendForDelivery(){
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            resolve("this is a sendForDelivery function")
        }, 2000)
    })
}

function orderCompleted(){
    return 'Mark order as completed'
}

let json = { id: 3, name: "Printer", price: 5000 };

/** Here Calling Promise*/

// createOrder(json)
//   .then(function (data) {
//     console.log("data  :", data);
//         createPayment().then((data) => {
//             console.log("createPayment :", data);
//             sendNotification().then((data) => {
//                 console.log("sendNotification ", data);
//                 sendForDelivery().then((data) => {
//                     console.log("sendForDelivery :", data);
//                     console.log("orderCompleted :", orderCompleted());
//                 });
//             });
//         }).catch(error=>{
//             console.log("payment error")
//         })
//   })
//   .catch(function (error) {
//     console.log("error :", error);
//   });


async function justForCall(){

    const order = await createOrder(json)
    console.log("order  :", order);

    const payment = await createPayment()
    console.log("payment :",payment)

    const notification = await sendNotification()
    console.log("notification :",notification)

    const delivery = await sendForDelivery()
    console.log("delivery :",delivery)

    const completed =  orderCompleted()
    console.log("completed :",completed)

}

justForCall()