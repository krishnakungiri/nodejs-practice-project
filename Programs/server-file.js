/**
 * server file
 */

const express = require("express");
const mongoose = require("mongoose");
const Persons = require("./src/Models1/persons.schema");
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const mongoUri =
  "mongodb+srv://krishna:krishna@cluster0.mfhov.mongodb.net/Practice";
mongoose.set("strictQuery", true);
mongoose.connect(
  mongoUri,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  (err, data) => {
    if (err) {
      console.log("error :", err);
    } else {
      console.log("mongodb Connection susccessfully");
    }
  }
);

app.listen(3000, function callback() {
  console.log("listening on 3000");
});

app.post("/app", async (req, res) => {
  const body = req.body;
  try {
    // const ObjectId = mongoose.Types.ObjectId;
    const chunks = await Persons.aggregate([
      //   {
      //     $match: { tags: {$size: 4} },
      //   },
      // {
      //     $group:{ _id: {companyCountry : '$company'}}
      // }
      // {
      //   $limit:100
      // },
      // {
      //   $project:{gen:'$gender'}
      // },
      // {$unwind: '$tags'},
      { $group: {
        _id:'$eyeColor',
        average:{
          $avg: '$age'
        }
      } },
    ]);
  
    return res.status(200).json({
      lenght: chunks.length,
      data: chunks,
    });
  } catch (error) {
    return res.status(500).json({ error: error });
  }
});



