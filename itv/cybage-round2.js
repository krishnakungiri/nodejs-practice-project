// const prm = new Promise((resolve, reject)=>{
//   if(true) return resolve(null)
//   reject(null)
// })

// prm.then(()=>{
//   console.log(" callback")
// })


// console.log("normal statement")

async function getUsers() {
    let arr = [
      { username: "john", email: "john@test.com" },
      { username: "jane", email: "jane@test.com" },
    ];
    return arr
}

async function findUser(username) {
const users= await getUsers()
const user = users.find((user) => user.username === username);
  return user
}
findUser("john").then((data)=>{
console.log("data :",data)
})
console.log("i am continuing with my work!");


// CREATE TABLE emp (
//       eid int,
//       ename varchar(255),
//       salary decimal(15,2),
//       state varchar(15),
//       designation varchar(15)
//   );

// select state, designation, count(designation)
// from emp group by state, designation

// select * from emp where MAX(salary)
// select * from emp order by salary desc limit 1