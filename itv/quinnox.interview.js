// const arr1 = [
//   {id: 1, name: 'a'},
//   {id: 2, name: 'b'},
//   {id: 3, name: 'c'},
//   {id: 4, name: 'd'},
//   {id: 5, name: 'e'}
// ];
// const arr2 = [
//   { id: 5 },
//   { id: 3 }
// ]

// const output = [
// { id: 5, name: 'e' },
// { id: 3, name: 'c' }
// ]

let finalArray =[]
for(let i=0; i<arr2.length; i++){
  let json = arr2[i]
  for(let j=0; j<arr1.length; j++){
    if(arr1[j].id == arr2[i].id){
      json['name']=arr1[j].name
    }
  }
  finalArray.push(json)
}

console.log("finalArray :" ,finalArray)

// var input = "HERE IS THE RESULT"
// output = "EREH SI EHT TLUSER"

let splittedString = input.split(' ')

let finalString =''
splittedString.forEach(element=>{
  let str = ''
  element = element.split('')
  element = element.reverse()
  element.forEach(ele=>{
    str = str + ele
  })
  finalString = finalString + ` ${str}`
})

console.log(finalString)



// const promise1 = Promise.reject(3);
// const promise2 = 42;
// const promise3 = new Promise((resolve, reject) => {
// setTimeout(resolve, 100, 'foo');
// })

// Promise.all([promise1, promise3]).then(data=>{
//   console.log("data  ",data)
// }).catch(err=>{
//   console.log("err ",err)
// })

Promise.resolve(1)
  .then((x) => x + 1)
  .then((x) => { throw new Error('My Error') })
  .catch(() => 1)
  .then((x) => x + 1)
  .then((x) => console.log(x))
  .catch(console.error)