// krishna kungiri - conver the text into camel case

let name = 'krishna kungiri'


function camelCase(input){
    let name = input.split('_')
    let str = ''
    for(let i=0; i<name.length; i++){
        if(i!=0){
            let temp = name[i].split('')
            temp[0] =temp[0].toUpperCase()
            str = str + temp.join('')
        }else{
            str += name[i]
        }
    }
    return str
}